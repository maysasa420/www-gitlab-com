---
layout: handbook-page-toc
title: Jamstack Single-Engineer Group
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
  {:toc .hidden-md .hidden-lg}

## Jamstack Single-Engineer Group

The Jamstack SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

This group's mission is to enable Frontend developers to build, deploy and manage externally facing, static websites using Jamstack architecture in a simple, configurable and scalable way. 

Frontend engineers should be able to use GitLab as the primary place to manage Jamstack Frontends.

## The Playing Field

Technology supporting Jamstack is a fast-growing, highly innovating field with many startups competing for market shares.

GitLab's solution is built on GitLab Pages and its solid CI/CD pipelines. This allows any client to grow seamlessly from a simple static site to complex, multi-faceted deployments without having to change the provider at any point. Competing in the Jamstack market potentially allows new Developers to have a shallow entry to GitLab's CI/CD world. That is, if we get the User Experience right.

GitLab is not an infrastructure provider. This is an advantage for established clients that may already pay for their own infrastructure. GitLab can easily support a wide range of infrastructure using CI/CD pipelines, so our goal is to make this as simple as possible. It's important that this scalability is conveyed from the very first onboarding steps to build trust with our users, even if most projects will stay small.

## Focus Areas

The key focus areas for the Jamstack SEG are:

1. [**Simplicity**](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics?label_name%5B%5D=Jamstack+Focus%3A%3ASimplicity) - It should be intuitive to use GitLab to deploy a static site. GitLab should be a great place to _just get started_ with the confidence that deployments can be fine-tuned and scaled later.
2. [**Configurability**](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics?label_name%5B%5D=Jamstack+Focus%3A%3AConfigurability) - With the `.gitlab-ci.yaml` powering the solution, it should be straightforward to support a wide range of deployment strategies.
3. [**Scalability**](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics?label_name%5B%5D=Jamstack+Focus%3A%3AScaleability) - GitLab should be able to support the entire page's lifecycle. From a quick-and-dirty one-man project to powering the websites of high-traffic global organizations with complex infrastructure needs.

## Epics

This is a non-exhaustive list in no particular order of concrete actions the Jamstack SEG takes to improve our quality in the above focus areas.

1. [Simplify GitLab's UI](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/2) to make it easier to get started with GitLab Pages for static sites, especially for existing repos
2. [Deploy static sites to a CDN](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/4) by default
3. [Introduce Versioning](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/3) for easier up- and downgrades, rolling updates and feature flags
4. Enable the user to [configure their own CDN](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/5)
5. [Support Server Side Rendering](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/6) for Incremental Static Regeneration and Distributed Persistent Rendering

## Backstory

The key to modern Jamstack Frontends are Websites built as Single Page Applications (SPA). Classic Javascript Frameworks like React or Vue.js build the entire HTML DOM on the client side, so to improve performance, they are supplemented by Rendering Engines (eg. Next.js, Nuxt.js) that use node.js to pre-render HTML before they are sent to the client's browser. Frameworks like Eleventy, Gatsby.js or Hugo are both Framework and Rendering Engine.

All frameworks have in common that there are three main approaches as to when the rendering happens:

1. During **Build** time, also known as _Static Site Generation_ (SSG), this is how GitLab Pages works now.
2. When a page is **Requested**, but instead of rendering in the client's browser, the page is pre-rendered by the server, known as _Server Side Rendering_ (SSR)
3. A mix of both, where pages are dynamically rendered once when requested for the first time, then cached, also known as Incremental Static Regeneration (ISR). This is usually supplemented with a step that pre-renders the most popular pages during build.

### Resources

- https://www.smashingmagazine.com/2021/05/evolution-jamstack/


## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329597](https://gitlab.com/gitlab-org/gitlab/-/issues/329597)
